package com.example.abbyytest.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.example.abbyytest.R;
import com.example.abbyytest.model.products.Product;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class GroceryProductViewHolder extends ChildViewHolder {
    private TextView mTextView;
    private TextView textCategory;

    public GroceryProductViewHolder(View itemView) {
        super(itemView);
        mTextView = itemView.findViewById(R.id.textView);
        textCategory = itemView.findViewById(R.id.textCategory);
    }

    public void bind(Product product) {
        mTextView.setText(product.getTitle());
        textCategory.setText(product.getCategory());
    }
}
