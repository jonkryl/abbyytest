package com.example.abbyytest.viewHolder;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abbyytest.R;
import com.example.abbyytest.model.GroceryList;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class GroceryViewHolder extends GroupViewHolder {
    private static final int FROM_DEGREES = 360;
    private static final int TO_DEGREES = 180;
    private static final float PIVOT_X = 0.5f;
    private static final float PIVOT_y = 0.5f;
    private static final int DURATION = 300;

    private TextView mTextView;
    private ImageView arrow;

    public GroceryViewHolder(View itemView) {
        super(itemView);

        mTextView = itemView.findViewById(R.id.textView);
        arrow = itemView.findViewById(R.id.arrow);
    }

    public void bind(GroceryList company) {
        mTextView.setText(company.getTitle());
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(FROM_DEGREES, TO_DEGREES, RELATIVE_TO_SELF, PIVOT_X, RELATIVE_TO_SELF, PIVOT_y);
        rotate.setDuration(DURATION);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(FROM_DEGREES, TO_DEGREES, RELATIVE_TO_SELF, PIVOT_X, RELATIVE_TO_SELF, PIVOT_y);
        rotate.setDuration(DURATION);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}
