package com.example.abbyytest.viewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    TextView textViewTitle;
    TextView textViewValue;

    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);
        textViewTitle = itemView.findViewById(R.id.textView);
        textViewValue = itemView.findViewById(R.id.textCategory);
    }

    public void bind(DetailItem detailItem) {
        textViewTitle.setText(detailItem.getTitle());
        textViewValue.setText(detailItem.getValue());
    }
}
