package com.example.abbyytest.model.products.books;

import android.os.Parcel;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;
import com.example.abbyytest.model.products.Product;

import java.util.List;

public class Book extends Product {
    private int page;

    public Book(String category, String title, int price, String barcode, int page) {
        super(category, title, price, barcode);
        this.page = page;
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    protected Book(Parcel in) {
        super(in);
        page = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(page);
    }

    @Override
    public List<DetailItem> getDetails() {
        List<DetailItem> list = super.getDetails();
        list.add(new DetailItem(R.string.page, String.valueOf(page)));
        return list;
    }
}
