package com.example.abbyytest.model.products.books;

import android.os.Parcel;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;

import java.util.List;

public class BookEsoterics extends Book {
    private int age;

    public BookEsoterics(String category, String title, int price, String barcode, int page, int age) {
        super(category, title, price, barcode, page);
        this.age = age;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(age);
    }

    protected BookEsoterics(Parcel in) {
        super(in);
        age = in.readInt();
    }

    public static final Creator<BookEsoterics> CREATOR = new Creator<BookEsoterics>() {
        @Override
        public BookEsoterics createFromParcel(Parcel in) {
            return new BookEsoterics(in);
        }

        @Override
        public BookEsoterics[] newArray(int size) {
            return new BookEsoterics[size];
        }
    };

    @Override
    public List<DetailItem> getDetails() {
        List<DetailItem> list = super.getDetails();
        list.add(new DetailItem(R.string.age, String.valueOf(age)));
        return list;
    }
}
