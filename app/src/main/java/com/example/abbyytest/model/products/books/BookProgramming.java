package com.example.abbyytest.model.products.books;

import android.os.Parcel;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;

import java.util.List;

public class BookProgramming extends Book {
    private String lang;

    public BookProgramming(String category, String title, int price, String barcode, int page, String lang) {
        super(category, title, price, barcode, page);
        this.lang = lang;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(lang);
    }

    protected BookProgramming(Parcel in) {
        super(in);
        lang = in.readString();
    }

    public static final Creator<BookProgramming> CREATOR = new Creator<BookProgramming>() {
        @Override
        public BookProgramming createFromParcel(Parcel in) {
            return new BookProgramming(in);
        }

        @Override
        public BookProgramming[] newArray(int size) {
            return new BookProgramming[size];
        }
    };

    @Override
    public List<DetailItem> getDetails() {
        List<DetailItem> list = super.getDetails();
        list.add(new DetailItem(R.string.lang, lang));
        return list;
    }
}
