package com.example.abbyytest.model.products;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;

import java.util.ArrayList;
import java.util.List;

public class Product implements Parcelable {
    private String title;
    private int price;
    private String barcode;
    private String category;

    public Product(String category, String title, int price, String barcode) {
        this.category = category;
        this.title = title;
        this.price = price;
        this.barcode = barcode;
    }

    protected Product(Parcel in) {
        title = in.readString();
        category = in.readString();
        barcode = in.readString();
        price = in.readInt();
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(category);
        dest.writeString(barcode);
        dest.writeInt(price);
    }

    public List<DetailItem> getDetails() {
        ArrayList<DetailItem> arrayList = new ArrayList<>();
        arrayList.add(new DetailItem(R.string.title, title));
        arrayList.add(new DetailItem(R.string.price, String.valueOf(price)));
        arrayList.add(new DetailItem(R.string.barcode, barcode));
        arrayList.add(new DetailItem(R.string.category, category));
        return arrayList;
    }
}
