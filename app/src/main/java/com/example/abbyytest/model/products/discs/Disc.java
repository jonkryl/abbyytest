package com.example.abbyytest.model.products.discs;

import android.os.Parcel;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;
import com.example.abbyytest.model.products.Product;

import java.util.List;

public class Disc extends Product {
    private String type;

    public Disc(String category, String title, int price, String barcode, String type) {
        super(category, title, price, barcode);
        this.type = type;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(type);
    }

    protected Disc(Parcel in) {
        super(in);
        type = in.readString();
    }

    public static final Creator<Disc> CREATOR = new Creator<Disc>() {
        @Override
        public Disc createFromParcel(Parcel in) {
            return new Disc(in);
        }

        @Override
        public Disc[] newArray(int size) {
            return new Disc[size];
        }
    };

    @Override
    public List<DetailItem> getDetails() {
        List<DetailItem> list = super.getDetails();
        list.add(new DetailItem(R.string.type, type));
        return list;
    }
}
