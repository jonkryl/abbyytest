package com.example.abbyytest.model.products.books;

import android.os.Parcel;

import com.example.abbyytest.R;
import com.example.abbyytest.model.DetailItem;

import java.util.List;

public class BookCooking extends Book {
    private String ingredient;

    public BookCooking(String category, String title, int price, String barcode, int page, String ingredient) {
        super(category, title, price, barcode, page);
        this.ingredient = ingredient;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(ingredient);
    }

    protected BookCooking(Parcel in) {
        super(in);
        ingredient = in.readString();
    }

    public static final Creator<BookCooking> CREATOR = new Creator<BookCooking>() {
        @Override
        public BookCooking createFromParcel(Parcel in) {
            return new BookCooking(in);
        }

        @Override
        public BookCooking[] newArray(int size) {
            return new BookCooking[size];
        }
    };

    @Override
    public List<DetailItem> getDetails() {
        List<DetailItem> list = super.getDetails();
        list.add(new DetailItem(R.string.ingredient, ingredient));
        return list;
    }
}
