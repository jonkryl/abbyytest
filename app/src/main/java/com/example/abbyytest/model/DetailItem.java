package com.example.abbyytest.model;

public class DetailItem {
    private int title;
    private String value;

    public DetailItem(int title, String value) {
        this.title = title;
        this.value = value;
    }

    public int getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }
}
