package com.example.abbyytest.model;


import com.example.abbyytest.model.products.Product;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class GroceryList extends ExpandableGroup<Product> {
    public GroceryList(String title, List<Product> items) {
        super(title, items);
    }
}
