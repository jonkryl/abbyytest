package com.example.abbyytest.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abbyytest.R;
import com.example.abbyytest.activity.ProductActivity;
import com.example.abbyytest.model.GroceryList;
import com.example.abbyytest.model.products.Product;
import com.example.abbyytest.viewHolder.GroceryProductViewHolder;
import com.example.abbyytest.viewHolder.GroceryViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class GroceryAdapter extends ExpandableRecyclerViewAdapter<GroceryViewHolder, GroceryProductViewHolder> {
    public GroceryAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public GroceryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_recyclerview_grocery, parent, false);
        return new GroceryViewHolder(v);
    }

    @Override
    public GroceryProductViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_recyclerview_product, parent, false);
        return new GroceryProductViewHolder(v);
    }

    @Override
    public void onBindChildViewHolder(final GroceryProductViewHolder holder, final int flatPosition, final ExpandableGroup group, final int childIndex) {
        final Product product = (Product) group.getItems().get(childIndex);
        holder.bind(product);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                context.startActivity(new Intent(context, ProductActivity.class).putExtra("Product", product));
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(GroceryViewHolder holder, int flatPosition, ExpandableGroup group) {
        final GroceryList company = (GroceryList) group;
        holder.bind(company);
    }
}

