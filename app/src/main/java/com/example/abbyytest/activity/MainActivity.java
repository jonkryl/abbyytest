package com.example.abbyytest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.abbyytest.R;
import com.example.abbyytest.adapter.GroceryAdapter;
import com.example.abbyytest.model.GroceryList;
import com.example.abbyytest.model.products.Product;
import com.example.abbyytest.model.products.books.BookCooking;
import com.example.abbyytest.model.products.books.BookEsoterics;
import com.example.abbyytest.model.products.books.BookProgramming;
import com.example.abbyytest.model.products.discs.Disc;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        GroceryAdapter adapter = new GroceryAdapter(createLists());
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<GroceryList> createLists() {
        ArrayList<GroceryList> groceryLists = new ArrayList<>();

        ArrayList<Product> bookProduct = new ArrayList<>();
        bookProduct.add(new BookCooking("Cooking", "Cooking Book", 157, "Barcode Cooking Book", 1500, "Potato"));
        bookProduct.add(new BookEsoterics("Esoterics", "Esoterics Book", 300, "Barcode Esoterics Book", 1700, 25));
        bookProduct.add(new BookProgramming("Programming", "Programming Book", 4000, "Barcode Programming Book", 2100, "Java"));

        GroceryList book = new GroceryList("Book", bookProduct);
        groceryLists.add(book);

        ArrayList<Product> discsProduct = new ArrayList<>();
        discsProduct.add(new Disc("Music", "Skillet", 100, "Barcode Skillet", "CD"));
        discsProduct.add(new Disc("Software", "AndroidStudio", 1000, "Barcode AndroidStudio", "DVD"));
        discsProduct.add(new Disc("Video", "Sasha Grey", 10, "Barcode Sasha Grey", "DVD"));

        GroceryList discs = new GroceryList("Discs", discsProduct);
        groceryLists.add(discs);
        return groceryLists;
    }
}
