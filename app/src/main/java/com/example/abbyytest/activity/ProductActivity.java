package com.example.abbyytest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.abbyytest.R;
import com.example.abbyytest.adapter.ProductAdapter;
import com.example.abbyytest.model.products.Product;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Product product = getIntent().getParcelableExtra("Product");

        RecyclerView recyclerView = findViewById(R.id.recyclerViewProduct);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ProductAdapter adapter = new ProductAdapter(product.getDetails());
        recyclerView.setAdapter(adapter);
    }
}
